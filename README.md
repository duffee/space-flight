# Space Flight

A long term, back burner project to gradually capture the requirements of interplanetary space flight in Perl.

## Class structure

''SpaceFlight''
* SpaceCraft
  * Dynamics (these are problems that need to be solved)
    * Propulsion
      * Engine Role
        - has thrust, fuel feed rate, exhaust speed, delta m, delta p, specific impluse
        - can fireEngine( %max, timeFiring )
      * PlasmaDrive (efficient, low impulse - good for corrections)
      * AntimatterDrive (extremely efficient - risk of explosion)
      * FusionDrive (high impulse - main drive option _when it becomes operation_)
    * Navigation
      * Position
      * Orientation
      * Velocity
      * Trajectory
      * GravitationalField
    * Communication
      * HighBandOptical
      * LowBandRadio
    * DecisionMaking
      * SituationalAwareness
      * CourseCorrections
      * EmergencyHandling
      * ExplorationPlanning
      * DataAnalysis
  * Schematics, Layout, Physical, Engineering
    * FuelTanks
    * Payload
      - 10 metric tonnes
    * Shielding
      - titanium/carbon nanotube ablative sheild for asteroid impacts
    * Engines
    * DamageControl
  * Exploration
    * OrbitalProbe
    * DescentModule
    * CometRendezvous
* Journey (destination, start position, finish position)
  * transfer orbits
* NavigationalObservations
  * AsteroidAvoidance

## References

I've been watching the Living Universe mini-series
